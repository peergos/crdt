package peergos.client;

import java.nio.file.*;
import java.util.*;

public class DirElement implements Element, Crdt<DirElement> {

    private final String name;
    private final Map<String, Element> children;

    public DirElement(String name, Map<String, Element> children) {
        this.name = name;
        this.children = children;
    }

    @Override
    public Type type() {
        return Type.Dir;
    }

    @Override
    public String name() {
        return name;
    }

    public Collection<Element> children() {
        return children.values();
    }

    public DirElement add(Path p, Type t) {
        int depth = p.getNameCount();
        if (depth == 0) {
            throw new IllegalStateException("Empty path adding element!");
        }
        String childName = p.getName(0).toString();
        if (depth > 1) {
            DirElement child = (DirElement) children.getOrDefault(childName, new DirElement(childName, Collections.emptyMap()));
            DirElement withDescendents = child.add(p.subpath(1, depth), t);
            return put(withDescendents);
        }
        return put(new File(childName));
    }

    public DirElement remove(Path p) {
        int depth = p.getNameCount();
        if (depth == 0) {
            throw new IllegalStateException("Empty path adding element!");
        }
        String childName = p.getName(0).toString();
        if (depth > 1) {
            DirElement child = (DirElement) children.get(childName);
            if (child == null)
                return this;
            DirElement withDescendents = child.remove(p.subpath(1, depth));
            return put(withDescendents);
        }
        return remove(childName);
    }

    private DirElement put(Element e) {
        HashMap<String, Element> updated = new HashMap<>(children);
        updated.put(e.name(), e);
        return new DirElement(name, updated);
    }

    private DirElement remove(String name) {
        HashMap<String, Element> updated = new HashMap<>(children);
        updated.remove(name);
        return new DirElement(name, updated);
    }
}
