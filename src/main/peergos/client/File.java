package peergos.client;

public class File implements Element {

    private final String name;

    public File(String name) {
        this.name = name;
    }

    @Override
    public Type type() {
        return Type.File;
    }

    @Override
    public String name() {
        return name;
    }
}
