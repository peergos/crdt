package peergos.client;

public interface Element {

    enum Type {File, Dir}

    Type type();

    String name();
}
